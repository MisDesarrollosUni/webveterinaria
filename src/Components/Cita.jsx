import React, { Fragment } from "react";
import { Link, withRouter } from "react-router-dom";
import clienteAxios from "../config/axios";
import Swal from "sweetalert2";

function Cita(props) {
    const { _id, nombre, propietario, fecha, horas, sintomas, telefono } =
        props.cita;
    if (!props.cita) {
        props.history.push("/");
        return null;
    }

    const eliminarCita = (id) => {
        Swal.fire({
            title: "¿Quieres eliminar la cita?",
            text: "No podrás recuperarlo",
            icon: "warning",
            showCancelButton: true,
            confirmButtonColor: "#3085d6",
            cancelButtonColor: "#d33",
            confirmButtonText: "Si, eliminar!",
        }).then((result) => {
            if (result.isConfirmed) {
                clienteAxios
                    .delete(`/pacientes/${id}`)
                    .then((response) => {
                        Swal.fire("Eliminado!", "Cita eliminada", "success");
                        props.history.push("/");
                        props.setConsultar(true);
                    })
                    .catch((err) => {
                        console.error(err);
                    });
            }
        });
    };

    return (
        <Fragment>
            <h1>Nombre cita: {nombre}</h1>

            <div className="container mt-5 py-5">
                <div className="row">
                    <div className="col-12 mb-5 d-flex justify-content-center">
                        <Link
                            to={"/"}
                            className="btn btn-success text-uppercase py-2 px-5 font-weight-bold"
                        >
                            Volver
                        </Link>
                    </div>

                    <div className="col-md-8 mx-auto">
                        <div className="list-group">
                            <div className="p-5 list group-item list-group-item action felx cloumn align-items-center">
                                <div className="d-flex w-100 justify-content-between mb-4">
                                    <h3 className="mb-3">{nombre}</h3>
                                    <small className="fecha-alta">
                                        {fecha} - {horas}
                                    </small>
                                </div>

                                <p className="mb-0">{sintomas}</p>
                                <div className="contacto py-3">
                                    <p>Dueño: {propietario}</p>
                                    <p>Dueño: {telefono}</p>
                                </div>

                                <div className="d-flex">
                                    <button
                                        type="button"
                                        className="text-uppercase py-2 px-5 font-weight-bold btn btn-danger col"
                                        onClick={() => {
                                            eliminarCita(_id);
                                        }}
                                    >
                                        Eliminar &times;
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </Fragment>
    );
}
export default withRouter(Cita);
