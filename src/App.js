import React, { useEffect, useState } from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";

import clienteAxios from "./config/axios";

import Cita from "./Components/Cita";
import NuevCita from "./Components/NuevCita";
import Pacientes from "./Components/Pacientes";

function App() {
    const [citas, setCitas] = useState([]);
    const [consultar, setConsultar] = useState(true);

    useEffect(() => {
        if (consultar) {
            const consultarAPI = () => {
                clienteAxios
                    .get("/pacientes")
                    .then((resp) => {
                        setCitas(resp.data);
                        setConsultar(false);
                    })
                    .catch((err) => console.error(err));
            };
            consultarAPI();
        }
    }, [consultar]);

    return (
        <Router>
            <Switch>
                <Route
                    exact
                    path="/"
                    component={() => (
                        <Pacientes citas={citas} setConsultar={setConsultar} />
                    )}
                />
                <Route
                    exact
                    path="/nueva"
                    component={() => <NuevCita setConsultar={setConsultar} />}
                />
                <Route
                    exact
                    path="/cita/:id"
                    render={(props) => {
                        const cita = citas.filter(
                            (cita) => cita._id === props.match.params.id
                        );
                        return (
                            <Cita cita={cita[0]} setConsultar={setConsultar} />
                        );
                    }}
                />
            </Switch>
        </Router>
    );
}

export default App;
